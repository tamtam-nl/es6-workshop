test('can be triggered when the incoming argument is undefined', () => {
  function getName(name = 'Mercury') {
    return name
  }

  expect(getName('Aaron')).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName(undefined)).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName(null)).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName()).toBe(/*ENTER YOUR GUESS HERE*/)
})

test(`aren't included in arguments`, () => {
  function getName(name = 'Mercury') {
    return arguments.length
  }

  expect(getName('Aaron')).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName(null)).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName()).toBe(/*ENTER YOUR GUESS HERE*/)
})

test('can trigger a function call', () => {
  let triggerCount = 0

  function getName(name = getDefault()) {
    return name
  }

  function getDefault() {
    triggerCount++
    return 'Mercury'
  }

  expect(triggerCount).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName('Aaron')).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName()).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(getName(undefined)).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(triggerCount).toBe(/*ENTER YOUR GUESS HERE*/)
})

test('catch non-specified params', () => {
  function resty(first, second, ...others) {
    return others
  }

  expect(resty().length).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(resty(1).length).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(resty(1, 2).length).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(resty(1, 2, 3).length).toBe(/*ENTER YOUR GUESS HERE*/)
  expect(
    resty(1, 2, 3, undefined, 5, undefined, 7, undefined, 9, 10).length,
  ).toBe(/*ENTER YOUR GUESS HERE*/)
})

/*
eslint
  no-unused-vars:0
  prefer-rest-params:0
*/
