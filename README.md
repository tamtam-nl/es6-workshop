# ES6-Workshop

## Welcome

Welcome to this ES6 workshop. Our goal is to help everyone make strides towards learning the newest ES6 syntax, and
how to use the latest constructs in JavaScript.

## Your Goal

This repo is full of failing tests. The only way to get them to pass it by using your newly acquired knowledge of ES6
and all of the new features that are part of this latest release. For each test, you will need to write/rewrite to code
using the latest ES6 syntax. Once all of your tests pass, you will want to move onto the next section.

## System Requirements

* [git][git]
* [Node][node] (at least version 6)

## Setup

You will need to start by cloning this repo, then you'll run a simple `setup` script which will install all the
dependencies and validate that you're ready to go. From your terminal, type:

```
npm i
```

# How To Run Tests

Once you have everything installed, you are ready to run some tests. You will need to edit the tests in the `exercises` directory, and make the
tests pass.

To run the tests, in your terminal run:

```
npm run test
```

To run a specific test file, in your terminal run:

```
npm run test -- exercises/01_block-scoping.test.js
```

This will start `jest` in its interactive watch mode ([learn more][watch-mode]). If you see a bunch of failures, make
sure you're in "only changed" mode by clicking the `o` key. There should not be any tests run at this point. Your
job is to go into each test file in `exercises` and make the tests pass. As you make changes, jest will run the file's
tests. Once you have written the required ES6 code, the tests will pass, and you can move on.

Also, during watch mode you can press the `p` key to filter for only the test file you want to run. You can even press the `t`
key to filter for only the tests you want to run (by their
test name). Note, that for these to work, you need to run all
the tests first. Hit the `a` key to do that.
